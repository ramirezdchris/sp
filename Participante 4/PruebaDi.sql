-- DROP DATABASE pruebaDi;
CREATE DATABASE pruebaDi;
USE pruebaDi;

CREATE TABLE cliente
(
	id_cliente INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(30) NOT NULL,
    apellido VARCHAR(30) NOT NULL,
    direccion VARCHAR(500) NOT NULL,
    fecha_nacimiento DATE NOT NULL,
    telefono  VARCHAR(30) NOT NULL,
    email VARCHAR(30) NOT NULL
);

CREATE TABLE modo_pago
(
	num_pago INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    otros_detalles VARCHAR(200) NOT NULL
);

CREATE TABLE categoria
(
	id_categoria INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    descripcion VARCHAR(200) NOT NULL    
);

CREATE TABLE producto
(
	id_producto INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    precio DOUBLE(10,2) NOT NULL,
    stock INT NOT NULL,
    id_categoria INT NOT NULL
);

CREATE TABLE factura
(
	id_factura INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	num_factura VARCHAR(20) NOT NULL,
    fecha DATE NOT NULL,
    id_cliente INT NOT NULL,
    num_pago INT NOT NULL
);

CREATE TABLE detalle
(
	num_detalle INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    cantidad INT NOT NULL,
    precio DOUBLE(10,2) NOT NULL,
    id_factura INT NOT NULL,
    id_producto INT NOT NULL
);


-- Tabla factura
ALTER TABLE factura ADD CONSTRAINT fk_factura_cliente FOREIGN KEY(id_cliente) REFERENCES cliente(id_cliente);
ALTER TABLE factura ADD CONSTRAINT fk_factura_modopago FOREIGN KEY(num_pago) REFERENCES modo_pago(num_pago);


-- Tabla producto
ALTER TABLE producto ADD CONSTRAINT fk_producto_categoria FOREIGN KEY(id_categoria) REFERENCES categoria(id_categoria);


-- Tabla detalle
ALTER TABLE detalle ADD CONSTRAINT fk_detalle_producto FOREIGN KEY(id_producto) REFERENCES producto(id_producto);
ALTER TABLE detalle ADD CONSTRAINT fk_detalle_factura FOREIGN KEY(id_factura) REFERENCES factura(id_factura);



-- Procedimiento almacenado para insertar factura
DELIMITER //
CREATE PROCEDURE sp_insercion_factura(IN num_factura VARCHAR(10),IN fecha DATE, IN cliente INT, IN num_pago INT)
BEGIN    
    INSERT INTO Factura VALUES(0, num_factura, fecha, cliente, num_pago);
END//
DELIMITER;



-- Trigger para disminuir el producto por la cantidad que se va a vender
DELIMITER //
CREATE TRIGGER tr_disminuye_stock AFTER INSERT ON detalle FOR EACH ROW
BEGIN
	UPDATE producto SET stock = stock - NEW.cantidad WHERE id_producto = NEW.id_producto;
END//



INSERT INTO cliente VALUES(0,'Christian','Ramirez','Colonia USAM','1997-06-16','2257-7777','chris@mail.com');
INSERT INTO modo_pago VALUES(0,'Credito','Por cuotas'),(0,'Al contado','Pago unico');

INSERT INTO categoria VALUES(0,'Calzado','Calzado de hombre y mujer'),(0,'Electrodomesticos','Equipos para el hogar'),(0,'Muebles','Para el hogar');





/*
SELECT * FROM Producto;
SELECT * FROM Factura;
SELECT * FROM Detalle;
SELECT * FROM Cliente;

SELECT @num = COUNT(num_factura) FROM Factura;

CALL sp_insercion_factura('2020-01-21',1,1);

CALL sp_MostrarCliente;

*/

/*
INSERT INTO Detalle VALUES(0,5,25.00,'JAVA000',1);
INSERT INTO Factura VALUES(0,'JAVA0001','2020-1-22',1,1);
DROP TRIGGER tr_disminuye_stock;
*/

-- SELECT * FROM Factura;

-- SELECT * FROM Factura ORDER BY id_factura DESC;