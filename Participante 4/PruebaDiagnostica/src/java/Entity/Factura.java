/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 *
 * @author christian.ramirezusa
 */
@Entity
@Table(name = "factura")
@NamedStoredProcedureQuery(name = "insert" , procedureName = "sp_insercion_factura" , 
        parameters = { @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class , name = "1"),
                       @StoredProcedureParameter(mode = ParameterMode.IN, type = Date.class , name = "2"),
                    @StoredProcedureParameter(mode = ParameterMode.IN, type = Integer.class , name = "3" ),
                   @StoredProcedureParameter(mode = ParameterMode.IN, type = Integer.class, name = "4")
})

@NamedQuery(name = "ultimoRegistro" , query = "SELECT F FROM Factura F ORDER BY F.id_factura DESC")
public class Factura implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)    
    private int id_factura;
    
    @Column(name = "num_factura")
    private String num_factura;

    @Temporal(TemporalType.DATE)
    @Column(name = "fecha")
    private Date fecha;

    @ManyToOne
    @JoinColumn(name = "id_cliente")
    private Cliente id_cliente;

    @ManyToOne
    @JoinColumn(name = "num_pago")
    private ModoPago num_pago;

    public int getId_factura() {
        return id_factura;
    }

    public void setId_factura(int id_factura) {
        this.id_factura = id_factura;
    }

    public String getNum_factura() {
        return num_factura;
    }

    public void setNum_factura(String num_factura) {
        this.num_factura = num_factura;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Cliente getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(Cliente id_cliente) {
        this.id_cliente = id_cliente;
    }

    public ModoPago getNum_pago() {
        return num_pago;
    }

    public void setNum_pago(ModoPago num_pago) {
        this.num_pago = num_pago;
    }

    
        
}
