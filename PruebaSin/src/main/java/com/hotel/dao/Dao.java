/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotel.dao;
import java.util.List;
/**
 *
 * @author christian.ramirezusa
 */
public interface Dao<T>{
    public void create(T t);
    public void remove(T t);
    public void edit(T t);
    public T find(Object t);
    public List<T> findAll();
}