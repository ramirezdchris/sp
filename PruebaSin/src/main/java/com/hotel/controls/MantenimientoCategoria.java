/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotel.controls;
import com.hotel.implement.CategoriaCRUD;
import com.hotel.models.Categoria;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
/**
 *
 * @author christian.ramirezusa
 */
@ManagedBean
@SessionScoped
public class MantenimientoCategoria implements Serializable{
    private CategoriaCRUD categImp;
    private List<Categoria> list;
    private Categoria categoria;
    private String mensaje="";
    
    public List<Categoria> getList() {        
        this.list = this.categImp.findAll();
        return list;
    }
    public void setList(List<Categoria> list) {
        this.list = list;
    }
    public Categoria getCategoria() {
        return categoria;
    }
    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    
    @PostConstruct
    public void init(){
        this.categoria = new Categoria();
        this.categImp = new CategoriaCRUD();
    }
    
    //Metodo guardar
    public void guardar(){
        try {
            
            this.categImp.create(this.categoria);
            
            System.out.println("Se agregó nueva categoria "+this.categoria.getNombre());
            this.mensaje="Agregado";
        } catch (Exception e) {
            this.mensaje="Error";            
        }
        FacesMessage msj=new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
}
